import React from "react";

const About = () => {
    return (
        <div className=" p-4 mt-8 grid grid-cols-1 lg:grid-cols-2 lg:mx-12 ">
            <div className=" p-2 flex  flex-col gap-[32px]     mx-auto">
                <h1 className="   font-Inter_Bold text-[48px] md:w-[440px] text-Black   leading-[110%] ">
                    About Finsweet Podcast
                </h1>
                <p className="md:w-[446px] font-Inter_Regular text-[16px] leading-[28px] text-Black ">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                    diam nonumy eirmod tempor invidunt ut labore et dolore magna
                    aliquyam erat, sed diam voluptua.
                </p>
                <div className="flex  ">
                    <button className=" font-Inter_Bold font-normal h-[48px] w-[154px] bg-Blue ">
                        Subscribe Now
                    </button>
                </div>
            </div>
        </div>
    );
};

export default About;
