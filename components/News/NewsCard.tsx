import Link from "next/link";
import React from "react";

const NewsCard = () => {
    return (
        <div className=" w-[320px] md:w-[416px] mx-auto ">
            <div className="   relative  h-[516px] bg-Off_white overflow-hidden  ">
                <div className=" px-[40px] py-[40px] ">
                    <h1 className=" text-[32px] font-Inter_Bold leading-[120%] text-Black  ">
                        Read our articles & news
                    </h1>
                    <Link
                        className=" text-[16px] font-Inter_SemiBold text-Blue hover:border-b transition-all "
                        href={`#`}
                    >
                        See More
                    </Link>
                </div>

                <p className=" absolute left-0 top-[16rem] rotate-[45deg] w-[75px] h-[233px] bg-Blue "></p>
                <p className="absolute right-[12rem] top-[8rem] rotate-[45deg] w-[75px] h-[614px] bg-Green "></p>
                <p className=" absolute right-[0rem] top-[9rem] rotate-[45deg] w-[75px] h-[614px] bg-Blue "></p>
            </div>
        </div>
    );
};

export default NewsCard;
