import Image from "next/image";
import Link from "next/link";
import React from "react";

import NewsCard from "./NewsCard";

const News = () => {
    return (
        <div className="  mt-[64px] container flex justify-center   ">
            <div className="grid lg:flex lg:justify-around lg:flex-wrap   gap-4">
                <NewsCard />

                <div className=" p-12 md:p-0 w-[416px]">
                    <div className=" w-full h-[320px]    ">
                        <Image
                            height={100}
                            width={100}
                            className="h-full w-full"
                            layout="responsive"
                            alt=""
                            src={`/image/bitcoins-and-u-s-dollar-bills-730547.png`}
                        />
                    </div>
                    <h2 className=" mt-[-3rem]  md:mt-[32px] mb-[16px] text-[24px] font-Inter_Bold leading-[125%] text-Black ">
                        Getting the first 100 customers for your business
                    </h2>
                    <p className="text-[16px] mb-[16px] font-Inter_Regular leading-[150%] text-Black ">
                        Lorem ipsum at vero eos et accusam et justo duo dolores
                        et ea rebum.
                    </p>
                    <p className=" flex items-center gap-[8px]">
                        <Link
                            href={`#`}
                            className=" hover:border-b transition-all  text-[16px] font-Inter_SemiBold leading-[150%] text-Blue "
                        >
                            Read Now
                        </Link>
                    </p>
                </div>
                <div className="w-[416px] p-12 md:p-0 ">
                    <div className=" w-full h-[320px]    ">
                        <Image
                            height={100}
                            width={100}
                            className="h-full w-full"
                            layout="responsive"
                            alt=""
                            src={`/image/top-view-of-assorted-gadgets-on-desk-3568520.png`}
                        />
                    </div>
                    <h2 className=" mt-[-3rem]  md:mt-[32px] mb-[16px] text-[24px] font-Inter_Bold leading-[125%] text-Black ">
                        Apparently we had reached a great height in the
                        atmosphere, ...
                    </h2>
                    <p className="text-[16px] mb-[16px] font-Inter_Regular leading-[150%] text-Black ">
                        Lorem ipsum at vero eos et accusam et justo duo dolores
                        et ea rebum.
                    </p>
                    <p className=" flex items-center gap-[8px]">
                        <Link
                            href={`#`}
                            className=" hover:border-b transition-all  text-[16px] font-Inter_SemiBold leading-[150%] text-Blue "
                        >
                            Read Now
                        </Link>
                    </p>
                </div>
            </div>
        </div>
    );
};

export default News;
