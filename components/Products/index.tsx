import Image from "next/image";
import Link from "next/link";
import React from "react";
import ProductsAvailableOnPlatform from "./ProductsAvailableOnPlatform";

const Products = () => {
    return (
        <div className="   ">
            <ProductsAvailableOnPlatform />

            <div className=" container ml-[-1.4rem] flex justify-center   ">
                <div className="mt-[146px]  lg:flex justify-between   w-[80%]  lg:w-[90%] px-6  ">
                    <div className="space-y-[16px] lg:space-y-0   ">
                        <h2 className="text-Black font-Inter_Bold text-[32px] leading-[120%]  ">
                            Recent Episodes
                        </h2>
                        <p className="text-[16px] md:w-[371px] md:h-[55px] font-Inter_Regular leading-[150%]  ">
                            Apparently we had reached a great height in the
                            atmosphere, for the sky was a dead black.
                        </p>
                    </div>
                    <button className=" transition-all duration-300 text-[16px] w-fit h-fit font-Inter_Medium md:place-self-end bg-Blue  py-[12px] px-[32px]   text-[#ffffff]  ">
                        See all episodes
                    </button>
                </div>
            </div>
            <div className=" mt-[64px] container flex justify-center   ">
                {/*  */}
                <div className=" grid lg:flex lg:justify-around lg:flex-wrap gap-4 ">
                    <div className=" p-12 md:p-0  w-[416px]">
                        <div className=" w-full h-[256px] relative  ">
                            <Image
                                height={100}
                                width={100}
                                className="h-full w-full"
                                layout="responsive"
                                alt=""
                                src={`/image/photo-of-people-sitting-beside-table-3182755.png`}
                            />

                            <svg
                                width="14"
                                height="20"
                                viewBox="0 0 14 20"
                                fill="#ffffff"
                                xmlns="http://www.w3.org/2000/svg"
                                className=" absolute top-[16px] right-[16px] w-[40px] h-[40px] rounded-[50%] bg-Blue p-2 "
                            >
                                <path
                                    d="M14 7V8C13.998 9.68185 13.3906 11.3068 12.2888 12.5775C11.187 13.8482 9.6646 14.6797 8 14.92V18H11V20H3V18H6V14.92C4.3354 14.6797 2.81295 13.8482 1.71118 12.5775C0.609407 11.3068 0.00197111 9.68185 0 8V7H2V8C2 9.32608 2.52678 10.5979 3.46447 11.5355C4.40215 12.4732 5.67392 13 7 13C8.32608 13 9.59785 12.4732 10.5355 11.5355C11.4732 10.5979 12 9.32608 12 8V7H14ZM7 11C7.79565 11 8.55871 10.6839 9.12132 10.1213C9.68393 9.55871 10 8.79565 10 8V3C10 2.20435 9.68393 1.44129 9.12132 0.87868C8.55871 0.316071 7.79565 0 7 0C6.20435 0 5.44129 0.316071 4.87868 0.87868C4.31607 1.44129 4 2.20435 4 3V8C4 8.79565 4.31607 9.55871 4.87868 10.1213C5.44129 10.6839 6.20435 11 7 11Z"
                                    fill="white"
                                />
                            </svg>
                        </div>
                        <h2 className=" mt-[-2rem]  md:mt-[32px] mb-[16px] text-[24px] font-Inter_Bold leading-[125%] text-Black ">
                            Ep 1: How to build a world-class business brand
                        </h2>
                        <p className="text-[16px] mb-[16px] font-Inter_Regular leading-[150%] text-Black ">
                            Lorem ipsum at vero eos et accusam et justo duo
                            dolores et ea rebum.
                        </p>
                        <p className=" flex items-center gap-[8px]">
                            <svg
                                width="10"
                                height="12"
                                viewBox="0 0 10 12"
                                fill="#ffffff"
                                xmlns="http://www.w3.org/2000/svg"
                                className="w-[32px]  h-[32px] bg-Blue p-2 pl-[10px] rounded-[50%]  "
                            >
                                <path
                                    d="M9.33337 5.99996L0.444486 11.6452L0.444487 0.354761L9.33337 5.99996Z"
                                    fill="white"
                                />
                            </svg>
                            <Link
                                href={`#`}
                                className=" hover:border-b transition-all  text-[16px] font-Inter_SemiBold leading-[150%] text-Blue "
                            >
                                Listen Now
                            </Link>
                        </p>
                    </div>

                    <div className=" p-12 md:p-0 w-[416px]">
                        <div className=" w-full h-[256px] relative  ">
                            <Image
                                height={100}
                                width={100}
                                className="h-full w-full"
                                layout="responsive"
                                alt=""
                                src={`/image/man.png`}
                            />

                            <svg
                                width="14"
                                height="20"
                                viewBox="0 0 14 20"
                                fill="#ffffff"
                                xmlns="http://www.w3.org/2000/svg"
                                className=" absolute top-[16px] right-[16px] w-[40px] h-[40px] rounded-[50%] bg-Blue p-2 "
                            >
                                <path
                                    d="M14 7V8C13.998 9.68185 13.3906 11.3068 12.2888 12.5775C11.187 13.8482 9.6646 14.6797 8 14.92V18H11V20H3V18H6V14.92C4.3354 14.6797 2.81295 13.8482 1.71118 12.5775C0.609407 11.3068 0.00197111 9.68185 0 8V7H2V8C2 9.32608 2.52678 10.5979 3.46447 11.5355C4.40215 12.4732 5.67392 13 7 13C8.32608 13 9.59785 12.4732 10.5355 11.5355C11.4732 10.5979 12 9.32608 12 8V7H14ZM7 11C7.79565 11 8.55871 10.6839 9.12132 10.1213C9.68393 9.55871 10 8.79565 10 8V3C10 2.20435 9.68393 1.44129 9.12132 0.87868C8.55871 0.316071 7.79565 0 7 0C6.20435 0 5.44129 0.316071 4.87868 0.87868C4.31607 1.44129 4 2.20435 4 3V8C4 8.79565 4.31607 9.55871 4.87868 10.1213C5.44129 10.6839 6.20435 11 7 11Z"
                                    fill="white"
                                />
                            </svg>
                        </div>
                        <h2 className=" mt-[-2rem]  md:mt-[32px] mb-[16px] text-[24px] font-Inter_Bold leading-[125%] text-Black ">
                            Ep 2: Getting the first 100 customers for your
                            business
                        </h2>
                        <p className="text-[16px] mb-[16px] font-Inter_Regular leading-[150%] text-Black ">
                            Lorem ipsum at vero eos et accusam et justo duo
                            dolores et ea rebum.
                        </p>
                        <p className=" flex items-center gap-[8px]">
                            <svg
                                width="10"
                                height="12"
                                viewBox="0 0 10 12"
                                fill="#ffffff"
                                xmlns="http://www.w3.org/2000/svg"
                                className="w-[32px]  h-[32px] bg-Blue p-2 pl-[10px] rounded-[50%]  "
                            >
                                <path
                                    d="M9.33337 5.99996L0.444486 11.6452L0.444487 0.354761L9.33337 5.99996Z"
                                    fill="white"
                                />
                            </svg>
                            <Link
                                href={`#`}
                                className=" hover:border-b transition-all  text-[16px] font-Inter_SemiBold leading-[150%] text-Blue "
                            >
                                Listen Now
                            </Link>
                        </p>
                    </div>
                    <div className=" p-12 md:p-0 w-[416px]">
                        <div className=" w-full h-[256px] relative  ">
                            <Image
                                height={100}
                                width={100}
                                className="h-full w-full"
                                layout="responsive"
                                alt=""
                                src={`/image/photo-of-women-having-conversation-3194524.png`}
                            />

                            <svg
                                width="14"
                                height="20"
                                viewBox="0 0 14 20"
                                fill="#ffffff"
                                xmlns="http://www.w3.org/2000/svg"
                                className=" absolute top-[16px] right-[16px] w-[40px] h-[40px] rounded-[50%] bg-Blue p-2 "
                            >
                                <path
                                    d="M14 7V8C13.998 9.68185 13.3906 11.3068 12.2888 12.5775C11.187 13.8482 9.6646 14.6797 8 14.92V18H11V20H3V18H6V14.92C4.3354 14.6797 2.81295 13.8482 1.71118 12.5775C0.609407 11.3068 0.00197111 9.68185 0 8V7H2V8C2 9.32608 2.52678 10.5979 3.46447 11.5355C4.40215 12.4732 5.67392 13 7 13C8.32608 13 9.59785 12.4732 10.5355 11.5355C11.4732 10.5979 12 9.32608 12 8V7H14ZM7 11C7.79565 11 8.55871 10.6839 9.12132 10.1213C9.68393 9.55871 10 8.79565 10 8V3C10 2.20435 9.68393 1.44129 9.12132 0.87868C8.55871 0.316071 7.79565 0 7 0C6.20435 0 5.44129 0.316071 4.87868 0.87868C4.31607 1.44129 4 2.20435 4 3V8C4 8.79565 4.31607 9.55871 4.87868 10.1213C5.44129 10.6839 6.20435 11 7 11Z"
                                    fill="white"
                                />
                            </svg>
                        </div>
                        <h2 className=" mt-[-2rem]  md:mt-[32px] text-[24px] font-Inter_Bold leading-[125%] text-Black ">
                            Ep 3: Should I raise money for my startup, or not?
                        </h2>
                        <p className="text-[16px] mb-[16px] font-Inter_Regular leading-[150%] text-Black ">
                            Lorem ipsum at vero eos et accusam et justo duo
                            dolores et ea rebum.
                        </p>
                        <p className=" flex items-center gap-[8px]">
                            <svg
                                width="10"
                                height="12"
                                viewBox="0 0 10 12"
                                fill="#ffffff"
                                xmlns="http://www.w3.org/2000/svg"
                                className="w-[32px]  h-[32px] bg-Blue p-2 pl-[10px] rounded-[50%]  "
                            >
                                <path
                                    d="M9.33337 5.99996L0.444486 11.6452L0.444487 0.354761L9.33337 5.99996Z"
                                    fill="white"
                                />
                            </svg>
                            <Link
                                href={`#`}
                                className=" hover:border-b transition-all  text-[16px] font-Inter_SemiBold leading-[150%] text-Blue "
                            >
                                Listen Now
                            </Link>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Products;
