import Link from "next/link";
import React from "react";

const Nav = () => {
    return (
        <div className="h-[72px]  px-4 lg:px-[90px] items-center flex justify-between ">
            <Link
                className=" hover:border-b border-Blue font-Inter_Bold "
                href={`/`}
            >{`{Finsweet`}</Link>
            <Link className=" hover:border-b border-Blue " href={`/about`}>
                About
            </Link>
        </div>
    );
};

export default Nav;
