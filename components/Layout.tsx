import React, { ReactNode } from "react";
import Footer from "./Footer";
import Nav from "./Nav";

const Layout = ({ children }: { children: ReactNode }) => {
    return (
        <>
            <Nav />
            <main className="mb-[250px]">{children}</main>
            <Footer />
        </>
    );
};

export default Layout;
