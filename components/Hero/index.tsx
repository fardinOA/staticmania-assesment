import React from "react";

const Hero = () => {
    return (
        <div className=" mt-8  w-screen flex items-center justify-center ">
            <div className="flex flex-col lg:flex-row  gap-[106px]   ">
                <div className="w-[70%]   mx-auto">
                    <div className=" flex  flex-col gap-[32px] ">
                        <h1 className="   font-Inter_Bold text-[48px] md:w-[440px] text-Black text-justify leading-[110%] ">
                            Become The Hero Of Your Own Story
                        </h1>
                        <p className="md:w-[446px] font-Inter_Regular text-[16px] leading-[28px] text-Black ">
                            Lorem ipsum dolor sit amet, consetetur sadipscing
                            elitr, sed diam nonumy eirmod tempor invidunt ut
                            labore et dolore magna aliquyam erat.
                        </p>
                        <div className="flex justify-center">
                            <input
                                placeholder="Enter Your Email Id"
                                className="w-[332px] h-[48px] px-4 bg-Off_white text-Gray font-Inter_Regular font-normal leading-[24px] text-[16px] "
                                type="text"
                            />
                            <button className=" font-Inter_Bold font-normal h-[48px] w-[154px] bg-Blue text-Off_white ">
                                Subscribe
                            </button>
                        </div>
                    </div>
                </div>
                <div className=" w-[70%] mx-auto ">
                    <div className="flex  items-center gap-[48px]  ">
                        <div className="w-[32px] h-[99px] bg-Blue  "></div>
                        <div className="w-[32px] h-[256px] bg-Green  "></div>
                        <div className="w-[32px] h-[178px] bg-Blue  "></div>
                        <div className="w-[32px] h-[99px] bg-Green  "></div>
                        <div className="w-[32px] h-[256px] bg-Blue  "></div>
                        <div className="w-[32px] h-[178px] bg-Green  "></div>
                        {/*  */}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Hero;
