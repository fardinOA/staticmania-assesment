# Static Mania Assessment

This is an assessment, based on next js and tailwind CSS. This assessment is for a mid-level front-end developer at static mania.

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/fardinOA/staticmania-assesment.git
```

Go to the project directory

```bash
  cd staticmania-assesment
```

Install dependencies

```bash
 yarn or npm i
```

Start the server

```bash
  yarn dev or npm run dev
```

## Demo

Visit the link to show the final output

https://staticmania-assesment.vercel.app

## Support

For support, email afnan.eu.cse@gmail.com or visit https://fardin-me.vercel.app
