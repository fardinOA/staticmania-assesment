/** @type {import('tailwindcss').Config} */
module.exports = {
    purge: [
        "./pages/**/*.{js,ts,jsx,tsx}",
        "./components/**/*.{js,ts,jsx,tsx}",
    ],
    content: [
        "./pages/**/*.{js,ts,jsx,tsx}",
        "./components/**/*.{js,ts,jsx,tsx}",
    ],
    theme: {
        extend: {},
        colors: {
            Green: "#1AD993",
            Aquamarine: "#ABFFE0",
            Blue: "#503AE7",
            Purple: "#832BC1",
            Black: "#14142B",
            Gray: "#AFB0B9",
            Off_white: "#F4F2FF",
        },
        fontFamily: {
            Inter_Bold: [`Inter-Bold`],
            Inter_Regular: [`Inter_Regular`],

            Inter_Medium: [`Inter_Medium`],
            Inter_Black: [`Inter_Black`],
            Inter_ExtraBold: [`Inter_ExtraBold`],
            Inter_ExtraLight: [`Inter_ExtraLight`],
            Inter_Light: [`Inter_Light`],
            Inter_SemiBold: [`Inter_SemiBold`],
            Inter_Thin: [`Inter_Thin`],
        },
    },
    plugins: [],
};
