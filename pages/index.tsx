import Head from "next/head";

import styles from "@/styles/Home.module.css";
import Hero from "@/components/Hero";
import Products from "@/components/Products";
import News from "@/components/News";

export default function Home() {
    return (
        <>
            <Head>
                <title>Home | Finsweet</title>
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1"
                />
                <link rel="icon" href="/logo.svg" />
            </Head>
            <div className={`overflow-x-hidden `}>
                <Hero />
                <div className="container mx-auto ">
                    <Products />
                    <News />
                </div>
            </div>
        </>
    );
}
