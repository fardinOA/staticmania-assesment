import About from "@/components/About";
import Head from "next/head";
import React from "react";

const index = () => {
    return (
        <>
            <Head>
                <title>About | Finsweet</title>
            </Head>
            <About />
        </>
    );
};

export default index;
